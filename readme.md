Implementation of Fast Koetter--Nielsen--Hoeholdt Interpolation algorithm
========================================

This is a publically available repository of an implementation which is
connected with the following publication:

* * *

**Fast Koetter--Nielsen--Hoeholdt Interpolation in the Guruswami--Sudan Algorithm**

Johan S. R. Nielsen

Presented at Fourteenth International Workshop on Algebraic and
Combinatorial Coding Theory, ACCT 2014

* * *

The code is written by me, Johan S. R. Nielsen

The code is documented in a self-contained fashion, but it should be
understandable for someone with a the article at hand.

It is written in [Sage], and will require this to run.
The source file(s) mention the latest version of Sage for which I have verified
they work. The code also requires installation of [Codinglib], my library of
functions and algorithms for algebraic coding theory. In fact, the bulk of the
algorithm and work might well be done by Codinglib, and as such, the
implementation here can be seen as calling examples of the
library. Before running the code in the files of this repository, one needs to
import Codinglib into the running Sage process:

    from codinglib import *

The code here is structured into `.sheet`-files. They are clear-text analogues
of the Sage Notebook's worksheets. Such a file is not meant to be imported as a
whole, but rather evaluated block by block in a Sage process. In a `sheet` file,
the blocks are delimited by lines starting with `###`, usually followed by a
one-line description of their contents. Possibly, the file is further
sub-structured by visibly larger whitespace. Usually, the first few blocks will
contain definitions, implementing the algorithms and helper functions. The
latter blocks then set up examples and objects for input in the algorithms.

The latter blocks of the sheet might also contain code for creating plots or
running the simulations included in the paper.

If you are using Emacs, then working with `.sheet`-files can be conveniently
accomplished using [sage-mode], where the file `sage-blocks.el`
contains block-specific functionality.

This code is released under the GPL v3 or later to conform with Sage's
licensing.

You are welcome to contact me for questions on the implementation or its usage,
or, of course, a discussion of the paper.

Regards,  
Johan S. R. Nielsen  
jsrn@jsrn.dk  
[jsrn.dk]

[Sage]: http://sagemath.org
[jsrn.dk]: http://jsrn.dk
[Codinglib]: http://jsrn.dk/codinglib
[sage-mode]: https://bitbucket.org/gvol/sage-mode
